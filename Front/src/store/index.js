import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    developers: [],
    tasks: [],
    task_status: [],
    task_type: [],
  },
  getters: {

  },
  mutations: {
    SET_DEVS(state, data) {
      state.developers = data;
    },
    SET_TASKS(state, data) {
      state.tasks = data;
    },
    SET_TASK_STATUS(state, data) {
      state.task_status = data;
    },
    SET_TASK_TYPE(state, data) {
      state.task_type = data;
    }
  },
  actions: {
    async getDevs(context) {
      const { data } = await axios.get(
        'http://localhost:8080/developers'
      );
      context.commit("SET_DEVS", data);
    },
    async getTasks(context) {
      const { data } = await axios.get(
        'http://localhost:8080/tasks'
      );
      context.commit("SET_TASKS", data);
    },
    async getTaskStatus(context) {
      const { data } = await axios.get(
        'http://localhost:8080/task_status'
      );
      context.commit("SET_TASK_STATUS", data);
    },
    async getTaskType(context) {
      const { data } = await axios.get(
        'http://localhost:8080/task_types'
      );
      context.commit("SET_TASK_TYPE", data);
    },

    async moveLeft(context,task_id) {
      await axios.patch('http://localhost:8080/tasks/'+task_id, { action: 'MOVE_LEFT' }).then(() => {
        context.dispatch('getTasks');
      });
    },
    async moveRight(context,task_id) {
      await axios.patch('http://localhost:8080/tasks/'+task_id, { action: 'MOVE_RIGHT' }).then(() => {
        context.dispatch('getTasks');
      });
    },

  },
  modules: {
  }
})
