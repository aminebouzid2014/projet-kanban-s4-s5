import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import DevComponent from '../components/Developers.vue'
import TasksComponent from '../components/Tasks.vue'
import AddTasksComponent from '../components/AddTask.vue'

Vue.use(VueRouter)



const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/developers',
    name: 'Developers',
    component: DevComponent
  },
  {
    path: '/tasks',
    name: 'Tasks',
    component: TasksComponent
  },
  {
    path: '/add_task',
    name: 'Add Tasks',
    component: AddTasksComponent
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },

]

const router = new VueRouter({
  routes
})

export default router

